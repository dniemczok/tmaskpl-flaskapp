from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<center><h1>Stany magazynowe Firma XXX ;)</h1></center><br><br><center><h2>{ "version" : "0.21" }</h2></center>'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, debug=True)


# docker build -t flaskapp .
# docker tag flaskapp:latest tmask/flaskapp
# docker push tmask/flaskapp

# docker run -d -p 3000:3000 tmask/flaskapp